public class App {
    public static void main(String[] args) throws Exception {
        //tạo đối tượng
        Account account1 = new Account("12354", "Son Chung");
        Account account2 = new Account("23465", "Vo Phạm", 1000);
        // //Ghi ra console
        // System.out.println(account1.toString());
        // System.out.println(account2.toString());
        //THêm balance
        account1.credit(2000);
        account2.credit(3000);
        //Ghi ra console
        // System.out.println(account1.toString());
        // System.out.println(account2.toString());
        //Giảm đi balance
        account1.debit(1000);
        account2.debit(5000);
         //Ghi ra console
        //  System.out.println(account1.toString());
        //  System.out.println(account2.toString());
        //Chuyển khoản
        account1.tranferTo(account2, 2000);
          //Ghi ra console
        //  System.out.println(account1.toString());
        //  System.out.println(account2.toString());
          //Chuyển khoản
        account2.tranferTo(account1, 2000);
           //Ghi ra console
           System.out.println(account1.toString());
           System.out.println(account2.toString());

    }
}
